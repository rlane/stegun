package ru.rlane.stegun.logic;

public class LogicException extends Exception {

	public LogicException(String message) {
		super(message);
	}
}
