package ru.rlane.stegun.logic;

import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;

import javax.inject.Singleton;
import java.io.InputStream;
import java.io.OutputStream;

@Singleton
public class Encryptor {

	public void doEncrypt(InputStream dataSource, int dataSourceSize, InputStream destinationImage, OutputStream output) throws LogicException {
		PngReader reader = new PngReader(destinationImage);
		checkFormat(reader);
		checkCapacity(dataSourceSize, reader);

		PngWriter writer = new PngWriter(output, reader.imgInfo);
		writer.copyChunksFrom(reader.getChunksList());

		for (int row = 0; row < reader.imgInfo.rows; row++) {
			ImageLineInt line = (ImageLineInt) reader.readRow();
			writer.writeRow(line);
		}

	}

	protected void checkCapacity(int dataSourceSize, PngReader destinationImageReader) throws CapacityException {
		int destinationImageCapacity = destinationImageReader.imgInfo.cols * destinationImageReader.imgInfo.rows * 3;
		if (dataSourceSize * 8 > destinationImageCapacity) {
			throw new CapacityException("Not enough capacity to inject data: need capaticy for " + dataSourceSize / 8 + " bytes, but has " + ((double)destinationImageCapacity / 8.0D));
		}
	}

	protected void checkFormat(PngReader destinationImageReader) throws InvalidFormatException {
		if (destinationImageReader.imgInfo.channels < 3) {
			throw new InvalidFormatException("Not enough channels: required 4 channel image");
		}
	}

	private static final Encryptor INSTANCE = new Encryptor();

	public static Encryptor getInstance() {
		return INSTANCE;
	}
}
