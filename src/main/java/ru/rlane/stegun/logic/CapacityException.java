package ru.rlane.stegun.logic;

public class CapacityException extends LogicException {

	public CapacityException(String message) {
		super(message);
	}
}
