package ru.rlane.stegun.logic;

public class InvalidFormatException extends LogicException {

	public InvalidFormatException(String message) {
		super(message);
	}
}
