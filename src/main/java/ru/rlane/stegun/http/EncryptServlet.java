package ru.rlane.stegun.http;

import ru.rlane.stegun.logic.Encryptor;
import ru.rlane.stegun.logic.LogicException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

@WebServlet(urlPatterns = "/encrypt.do")
public class EncryptServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isMultipart(request)) {
			returnSingleMessage(response, "Request type must be multipart/form-data");
			return;
		}
		Part dataSource = request.getPart("source");
		Part destinationImage = request.getPart("destination");
		// todo check parts and streams
		try {
			Encryptor.getInstance().doEncrypt(dataSource.getInputStream(), (int)dataSource.getSize(), destinationImage.getInputStream(), response.getOutputStream());
			response.setContentType("application/octet-stream");
			// todo response.setContentLength()
		} catch (LogicException e) {
			returnSingleMessage(response, "Exception occured during encryption: " + e.getLocalizedMessage());
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		returnSingleMessage(response, "Use method POST");
	}

	protected boolean isMultipart(HttpServletRequest request) {
		return "multipart/form-data".equals(request.getContentType());
	}

	protected void returnSingleMessage(HttpServletResponse response, String message) throws IOException {
		response.getWriter().println(message);
		response.getWriter().flush();
	}
}
